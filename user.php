<?php

require_once("connection.php");

if(!isset($_COOKIE['user_id']))
{
    header('Location: http://3660project.employike.com/login.php');
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ClassVote > Home</title>
    <meta name="keywords" content="shoes store, free template, ecommerce, online shop, website templates, CSS, HTML" />
    <meta name="description" content="Shoes Store is a free ecommerce template provided by templatemo.com" />
    <link href="templatemo_style.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />

    <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/ddsmoothmenu.js">

        /***********************************************
         * Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
         * This notice MUST stay intact for legal use
         * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
         ***********************************************/

    </script>

    <script type="text/javascript">

        ddsmoothmenu.init({
            mainmenuid: "top_nav", //menu DIV id
            orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
            classname: 'ddsmoothmenu', //class added to menu's outer DIV
            //customtheme: ["#1c5a80", "#18374a"],
            contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
        })

    </script>

</head>

<body>

<div id="templatemo_body_wrapper">
    <div id="templatemo_wrapper">

        <div id="templatemo_header">
            <div id="site_title">
                <h1><a href="http://3660project.employike.com" rel="nofollow">Class Rating System<br />University of Lethbridge</a></h1></div>
            <div id="header_right">
                <?php

                if(!isset($_COOKIE['fb_at']))
                {
                    echo "<a href='login.php'><img src='images/active_404@2x.png' alt='Login with Facebook'></a></p>";
                }
                else
                {
                    echo "<a href='myaccount.php'><img src='images/active2_404@2x.png' alt='My Account'></a></p>";
                }

                ?>
            </div>
            <div class="cleaner"></div>
        </div> <!-- END of templatemo_header -->

        <div id="templatemo_menubar">
            <div id="top_nav" class="ddsmoothmenu">
                <ul>
                    <li><a href="index.php" class="selected">Home</a></li>
                    <li><a href="classes.php">Classes</a></li>
                </ul>
                <br style="clear: left" />
            </div> <!-- end of ddsmoothmenu -->
            <div id="templatemo_search">
                <form action="search.php" method="get">
                    <input type="text" value="" name="subject" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                    <input type="submit" name="Search" value=" " alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
                </form>
            </div>
        </div> <!-- END of templatemo_menubar -->

        <div id="templatemo_main">
            <div id="sidebar" class="float_l">
                <div class="sidebar_box"><span class="bottom"></span>
                    <h3>Categories</h3>
                    <div class="content">
                        <ul class="sidebar_list">
                            <?php
                            $query = mysql_query("SELECT DISTINCT(subject) FROM  COURSES;");

                            while ($data = mysql_fetch_assoc($query)) {
                                echo "<li class='first'><a href='classes.php?subject=".$data['subject']."'>".$data['subject']."</a></li>";
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content" class="float_r">
                <h1>User Profile</h1>
                <?php
                if(isset($_GET['id'])) {
                    $id = $_GET['id'];

                $query0 = mysql_query("select U.user_id, fb_email, count(*) as count from USERS U, RATINGS R where U.user_id='".$id."' and U.user_id=R.user_id");
                if ($query0 && mysql_num_rows($query0) != 0) {
                    $data = mysql_fetch_assoc($query0);

                 ?>
                <strong>User Email:</strong> <?=$data['fb_email']?>
                <table width="680px" cellspacing="0" cellpadding="5">
                    <tr bgcolor="#ddd">
                        <th width="220" align="left">Class</th>
                        <th width="310" align="left">Comment</th>
                        <th width="80" align="right">My Rating</th>
                    </tr>
<?
                        $user = $_GET['id'];
                        $query = mysql_query("SELECT COURSES.course_id, subject, number, comment, rating from COURSES, RATINGS WHERE COURSES.course_id=RATINGS.course_id AND RATINGS.user_id='".$user."' group by COURSES.course_id;");

                        while ($data = mysql_fetch_assoc($query)) {
                            echo "<tr><td align='left'>".$data['subject']." ".$data['number']."</td><td align='left'>".$data['comment']."</td><td align='right'><p class='class_detail'>".$data['rating']."</p></td><td></td></tr>";
                        }

                        $query = mysql_query("SELECT ROUND(avg(rating), 1) AS rating FROM RATINGS WHERE user_id='".$user."';");

                        while ($data = mysql_fetch_assoc($query)) {
                            echo "<tr><td colspan='2' align='right' height='30px'></td><td align='right' style='background:#ddd; font-weight:bold'>Average</td><td align='right' style='background:#ddd; font-weight:bold'><p class='class_detail'>".$data['rating']."</p></td><td></td><td></td></tr>";
                        }
                        mysql_close();
                    } else {
                        echo "No user with that profile!";
                    }
} else {
echo "Profile ID can't be blank.";
}

                    ?>
                </table>
            </div>
            <div class="cleaner"></div>
        </div> <!-- END of templatemo_main -->

        <div id="templatemo_footer">
            <p><a href="#">Home</a> | <a href="#">Classes</a> | <a href="#">Instructors</a> | <a href="#">Contact Us</a>
            </p>

            Copyright © 2014 <a href="#">ClassVote</a> | <a rel="nofollow" href="http://www.templatemo.com/preview/templatemo_367_shoes">Theme</a> by <a href="http://www.templatemo.com" rel="nofollow" target="_parent" title="free css templates">templatemo</a>
        </div> <!-- END of templatemo_footer -->

    </div> <!-- END of templatemo_wrapper -->
</div> <!-- END of templatemo_body_wrapper -->


<script type='text/javascript' src='js/logging.js'></script>
</body>
</html>
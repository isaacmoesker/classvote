<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClassVote > Home</title>
<meta name="keywords" content="shoes store, free template, ecommerce, online shop, website templates, CSS, HTML" />
<meta name="description" content="Shoes Store is a free ecommerce template provided by templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "top_nav", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

</head>

<body>

<div id="templatemo_body_wrapper">
<div id="templatemo_wrapper">

	<div id="templatemo_header">
    	<div id="site_title">
    	  <h1><a href="http://3660project.employike.com" rel="nofollow">Class Rating System<br />University of Lethbridge</a></h1></div>
        <div id="header_right">
        <?php
			
			if(!isset($_COOKIE['user_id']))
			{
				echo "<a href='login.php'><img src='images/active_404@2x.png' alt='Login with Facebook'></a></p>";
			}
			else
			{
				echo "<a href='myaccount.php'><img src='images/active2_404@2x.png' alt='My Account'></a></p>";
			}
			
			?>
		</div>
        <div class="cleaner"></div>
    </div> <!-- END of templatemo_header -->
    
    <div id="templatemo_menubar">
    	<div id="top_nav" class="ddsmoothmenu">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="classes.php" class="selected">Classes</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of ddsmoothmenu -->
        <div id="templatemo_search">
            <form action="search.php" method="get">
               <input type="text" value="" name="subject" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
              <input type="submit" name="Search" value=" " alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>
    </div> <!-- END of templatemo_menubar -->
    
    <div id="templatemo_main">
    	<div id="sidebar" class="float_l">
        	<div class="sidebar_box"><span class="bottom"></span>
            	<h3>Categories</h3>   
                <div class="content"> 
                	<ul class="sidebar_list">
                        <?php
                        ini_set( "display_errors", 0);
                        require_once("connection.php");
                        $query = mysql_query("SELECT DISTINCT(subject) FROM  COURSES;");
                        
                        while ($data = mysql_fetch_assoc($query)) {
                            echo "<li class='first'><a href='classes.php?subject=".$data['subject']."'>".$data['subject']."</a></li>";
                            }
                        ?>
                    </ul>
                </div>
            </div>
    	</div>
    	<div id="content" class="float_r faqs">
<?php
$id = mysql_real_escape_string($_GET['id']);
$query = mysql_query("select COURSES.course_id, name, instructor, subject, number, ROUND(avg(rating),1) as rating from RATINGS, COURSES WHERE COURSES.course_id=".$id." AND COURSES.course_id=RATINGS.course_id group by COURSES.course_id");
                        
while ($data = mysql_fetch_assoc($query)) {
	echo "<h1>".$data['subject']." ".$data['number']."</h1><h5>".$data['name']."</h5><h5>".$data['instructor']."</h5><p class='class_detail'>".$data['rating']."</p>";
}

$query = mysql_query("SELECT * FROM RATINGS WHERE course_id=".$id." ORDER BY DATE(date) DESC;");
                        
while ($data = mysql_fetch_assoc($query)) {
	echo "<blockquote><p class='class_detail'>".$data['rating']." <span style='font-weight:normal; font-size: 12px; color:#999;'>/10</span></p><h5>".$data['comment']."</h5>".$data['date']."</blockquote>";
}
mysql_close();
?>
      </div>
    	<div class="cleaner"></div>
    </div> <!-- END of templatemo_main -->
    
    <div id="templatemo_footer">
    	<p><a href="#">Home</a> | <a href="#">Classes</a> | <a href="#">Instructors</a> | <a href="#">Contact Us</a>
		</p>

    	Copyright © 2014 <a href="#">ClassVote</a> | <a rel="nofollow" href="http://www.templatemo.com/preview/templatemo_367_shoes">Theme</a> by <a href="http://www.templatemo.com" rel="nofollow" target="_parent" title="free css templates">templatemo</a>
    </div> <!-- END of templatemo_footer -->
    
</div> <!-- END of templatemo_wrapper -->
</div> <!-- END of templatemo_body_wrapper -->


<script type='text/javascript' src='js/logging.js'></script>
</body>
</html>
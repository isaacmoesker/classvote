# CPSC 3660 - Project Proposal #
Social Course Registration Software - Web-based Course Registration System with Social Networking Elements 

## AUTHORS ##
James Palawaga, Isaac Moesker

## ABSTRACT ##
Over the past decade, almost every aspect of life has evolved into some sort of social interaction, thanks to social networking. Today, every business has had to develop an online social presence in order to reach out to their customers. Many systems today still do not have social functionality and for that reason, they are lacking potential. Most course registration software falls under this category. In order to exemplify the effectiveness of social networking, the design and implementation of a social online course registration system is proposed for this project. The website will allow students to log in with their Facebook account and be able to leave course ratings for other students to view in the future. 

## DESCRIPTION OF PROPOSED SYSTEM ##
In this project, a web-based social course registration system is to be designed and implemented using MySQL, HTML, PHP, and JavaScript. The main aim is to utilize the advantages of social network based development to bring people closer together when choosing courses. 

### Student Registration ###
Students will be asked to provide their Facebook login credentials. The student’s Facebook email is then stored in the “Users” table. 

### Course Ratings ###
Students can leave course reviews for future students to see. The review will be stored in the “Reviews” table along with the CRN and reviewer’s name. Course ratings will appear next to the course. 

### User Profile ###
The user will be able to see reviews they have left. 

## PROCESS ##
This section describes the software life-cycle that we intend to follow for this project. An iterative approach will be used such that the requirements and design will be refined during the implementation and testing phases. Moreover, an incremental approach will be adopted to first create a basic system and then, later on, enhance it with new functionalities as the system is being developed. 

### Requirements and Analysis ###
The functional requirements of the system will be gathered at this stage. This includes inception and consequent elaboration on all the web services that the system will provide to the students. 

### Architectural Design ###
The next step is to come up with the appropriate architectural model for the system. An Entity-relationship Diagram, a Relational Model, and a Functional Design will be developed at this stage. This will simply be an abstract view of the overall structure of the system.

### Implementation & Testing ###
In this phase, the system will be coded and tested. The client side of the system will be implemented using HTML and JavaScript, while the server side will be created using PHP. Finally, the portion of the database system will also be created for testing and demo purposes using the on-campus MySQL.
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ClassVote > Home</title>
<meta name="keywords" content="shoes store, free template, ecommerce, online shop, website templates, CSS, HTML" />
<meta name="description" content="Shoes Store is a free ecommerce template provided by templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="nivo-slider.css" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "top_nav", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

</head>

<body>

<div id="templatemo_body_wrapper">
<div id="templatemo_wrapper">

	<div id="templatemo_header">
    	<div id="site_title">
    	  <h1><a href="http://3660project.employike.com" rel="nofollow">Class Rating System<br />University of Lethbridge</a></h1></div>
        <div id="header_right">
        <?php
			
			if(!isset($_COOKIE['user_id']))
			{
				echo "<a href='login.php'><img src='images/active_404@2x.png' alt='Login with Facebook'></a></p>";
			}
			else
			{
				echo "<a href='myaccount.php'><img src='images/active2_404@2x.png' alt='My Account'></a></p>";
			}
			
			?>
		</div>
        <div class="cleaner"></div>
    </div> <!-- END of templatemo_header -->
    
    <div id="templatemo_menubar">
    	<div id="top_nav" class="ddsmoothmenu">
            <ul>
                <li><a href="index.php" class="selected">Home</a></li>
                <li><a href="classes.php">Classes</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of ddsmoothmenu -->
        <div id="templatemo_search">
            <form action="search.php" method="get">
                <input type="text" value="" name="subject" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
              <input type="submit" name="Search" value=" " alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>
    </div> <!-- END of templatemo_menubar -->
    
    <div id="templatemo_main">
    	<div id="sidebar" class="float_l">
        	<div class="sidebar_box"><span class="bottom"></span>
            	<h3>Categories</h3>   
                <div class="content"> 
                	<ul class="sidebar_list">
                        <?php
                        ini_set( "display_errors", 0);
                        require_once("connection.php");
                        $query = mysql_query("SELECT DISTINCT(subject) FROM  COURSES;");
                        
                        while ($data = mysql_fetch_assoc($query)) {
                            echo "<li class='first'><a href='classes.php?subject=".$data['subject']."'>".$data['subject']."</a></li>";
                            }
                        mysql_close();
                        ?>
                    </ul>
                </div>
            </div>
    	</div>
    	<div id="content" class="float_r faqs">
    	  <h1>Welcome!</h1>
    	  <h5>What is ClassVote?</h5>
    	  <p>ClassVote is a class rating system for the university of Lethbridge. Here is the basic idea, we encourage users to login with their Facebook credentials and leave review for future students to understand what a class involves (difficulty, interest, etc). You're probably wondering why we want you to login with your Facebook credentials right? Well, if you decide to login with Facebook then you will be able to view which courses your friends are enrolled in and reviews they have left.</p>
    	  <h5>What is your privacy policy?</h5>
    	  <p>This website respects your privacy and ensure that  you understand what information we need, and what  information you can choose to share with us and with our marketing partners.  For complete information on our privacy policy, please visit our <a href="#">Privacy Policy</a> page.</p>
  	  </div>
    	<div class="cleaner"></div>
    </div> <!-- END of templatemo_main -->
    
    <div id="templatemo_footer">
    	<p><a href="#">Home</a> | <a href="#">Classes</a> | <a href="#">Instructors</a> | <a href="#">Contact Us</a>
		</p>

    	Copyright © 2014 <a href="#">ClassVote</a> | <a rel="nofollow" href="http://www.templatemo.com/preview/templatemo_367_shoes">Theme</a> by <a href="http://www.templatemo.com" rel="nofollow" target="_parent" title="free css templates">templatemo</a>
    </div> <!-- END of templatemo_footer -->
    
</div> <!-- END of templatemo_wrapper -->
</div> <!-- END of templatemo_body_wrapper -->


<script type='text/javascript' src='js/logging.js'></script>
</body>
</html>
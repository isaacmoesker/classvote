<?php
require_once('connection.php');

$id = '';

if (isset($_REQUEST['signed_request']) && !(empty($_REQUEST['signed_request']))) {
    $data         =   parse_signed_request($_REQUEST['signed_request'], $config['secret']);
    $id   =   $data['user_id'];
} else {
    $id = mysql_real_escape_string($_GET['id']);
}

$query = mysql_query("delete from USERS where user_id='".$id."'");

if ($query && mysql_affected_rows() > 0) {
    $text = "User profile has been deleted";
  
    $response = $facebook->api(
        "/me/permissions",
        "DELETE"
    );

} else {
	$text = "Error occured deleting user profile.";
}

$facebook->destroySession();

setcookie("fb_at", "", time()-360000000000000);
setcookie("user_id", "", time()-360000000000000);

?>

<!DOCTYPE html>
<head>
	<title>Delete User Profile</title>
</head>
<body>
	<p>
        <meta http-equiv="refresh" content="0; url=index.php" />
	</p>
</body>
</html>
